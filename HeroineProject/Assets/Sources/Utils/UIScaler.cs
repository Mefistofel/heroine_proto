﻿using UnityEngine;
using System.Collections;

public class UIScaler : MonoBehaviour {
	
	[SerializeField]public float needScale = 1f; // fill from editor
	[SerializeField]public float normalPixelsInElement = 50f; // fill from editor
	RectTransform selfRectTransform;
	Rect currentRect = new Rect ();

	void Start () {
		selfRectTransform = GetComponent<RectTransform> ();
	}

	void Update () {
		if (selfRectTransform != null) {
			if(!selfRectTransform.rect.Equals(currentRect)) {
				currentRect = selfRectTransform.rect;
				selfRectTransform.localScale = Vector3.one * (currentRect.height / normalPixelsInElement) * needScale;
			}
		}
	}
}
