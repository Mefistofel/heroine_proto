using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
[RequireComponent (typeof(AudioListener))]
public class Music : MonoBehaviour {
	
	public static AudioSource currentSource = null;
	static float currentSound = 1;
	public const float MAXIMUM_VOLUME = 0.5f;
	public AudioSource source;
	
	Action action = null;
	
	public static bool musicEnable{
		get {
			return (currentSound > MAXIMUM_VOLUME * 0.5f);
		}
		set {
			//TODO SOUND
			if (value != musicEnable) {
				if (value) {
					if (currentSound < MAXIMUM_VOLUME * 0.5f) {
						if (currentSource != null) {
							currentSource.volume = MAXIMUM_VOLUME;
							currentSource.Play();
						}
					}
					currentSound = MAXIMUM_VOLUME;
				} else {
					currentSound = 0;
				}
				if (currentSource != null) {
					currentSource.volume = currentSound;
				}
			}
		}
	}
	
	public static Music menu;
	public static Music game;
	
	public float loopStart = 0;
	
	bool playing = false;
	
	void Awake () {
		game = GameObject.Find("game_music").GetComponent<Music>();
		menu = GameObject.Find("menu_music").GetComponent<Music>();
		
		Music.game.SetAction(
		delegate(){
			Music.game.Play();	
		});
		Music.menu.SetAction(
		delegate(){
			Music.menu.Play();	
		});
	}
	
	public static void SetVolume(float volume) {
		currentSound = volume;
		if (currentSource != null) {
			currentSource.volume = currentSound;
		}
	}
	
	public AudioSource Play (bool inMainThread = true, float volume = -1) {
		if (volume < 0) {
			volume = currentSound;
		}
		if (audio != null) {
			if (!audio.isPlaying) {
				audio.Play();
				playing = true;
				audio.volume = volume;
				if (inMainThread) {
					currentSource = audio;
					currentSource.volume = volume;
				}
			}
			return audio;
		}
		return null;
	}
	
	public void Stop () {
		if (audio != null) {
			if (audio.isPlaying) {
				audio.Stop();
				playing = false;
			}
		}
	}

	public void SetAction (Action newAction) {
		action = newAction;
	}

	float volume = 0;
	public float needVolume = 0;
	void Update () {
		if (!audio.isPlaying && playing) {
			playing = false;
			if (action != null) {
				action();
			}
		}
		if (volume != needVolume && audio != null) {
			if (volume > needVolume) {
				volume -= Time.deltaTime * 0.2f;
				if (volume <= needVolume) {
					volume = needVolume;
					if (volume <= 0) {
						Stop ();
					}
				}
				audio.volume = volume;
			} else {
				volume += Time.deltaTime * 0.2f;
				if (volume >= needVolume) {
					volume = needVolume;
				}
				audio.volume = volume;
			}
		}
	}
}
