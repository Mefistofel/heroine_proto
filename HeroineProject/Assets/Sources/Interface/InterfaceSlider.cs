using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InterfaceSlider : MonoBehaviour {
	const float SCREEN_RAY_LENGTH = 50f;

	public Transform controledTransform = null;
	public float unitsOnHeight = 12f;
	public bool horisontalMove = true;
	public bool verticalMove = true;

	public bool useBorders = true;
	public Rect borders = new Rect(0, 0, 1, 1);
	public bool useInertion = false;
	public float dexelerationFactor = 0.95f; // dexeleration - unit per second
	
	public Vector3 size = Vector3.one;

	bool isTouched = false;
	Vector2 currentPosition;
	Vector2 velocity;

	BoxCollider buttonCollider;
	int touchId = -1;

	protected void Register() {
		Debug.Log (this.GetType().Name);
	}

	void Start () {
		buttonCollider = GetComponent<BoxCollider>();
		if (buttonCollider == null) {
			buttonCollider = gameObject.AddComponent<BoxCollider>();		
		}
		buttonCollider.size = size;
	}

	void StartTouch(Vector2 position) {
		currentPosition = position;
		isTouched = true;
		velocity = Vector3.zero;
	}

	void UpdateTouch(Vector2 position) {
		Vector2 change = position - currentPosition;
		currentPosition = position;
		if (!horisontalMove) {
			change.x = 0;
		}
		if (!verticalMove) {
			change.y = 0;
		}
		velocity = velocity * 0.05f + (change / (float)Screen.height / transform.lossyScale.y * unitsOnHeight) * 0.95f;
	}

	void MoveObject() {
		if (controledTransform != null) {
			Vector3 objectPosition = controledTransform.localPosition;
			objectPosition += (Vector3)velocity;
			if (useBorders) {
				if (objectPosition.x > borders.xMax) {
					objectPosition.x = borders.xMax;
				}
				if (objectPosition.y > borders.yMax) {
					objectPosition.y = borders.yMax;
				}
				if (objectPosition.x < borders.xMin) {
					objectPosition.x = borders.xMin;
				}
				if (objectPosition.y < borders.yMin) {
					objectPosition.y = borders.yMin;
				}
			}
			controledTransform.localPosition = objectPosition;
			// TODO remake
			if (useInertion) {
				velocity = velocity * dexelerationFactor;
			}
		}
	}

	void EndTouch(Vector2 position) {
		UpdateTouch(position);
		currentPosition = position;
		isTouched = false;
		if (!useInertion) {
			velocity = Vector3.zero;
		}
	}

	void Update () {
		if (!isTouched) {
			for (int i = 0; i < Input.touches.Length; i++) {
				if (Input.touches [i].phase == TouchPhase.Began) {
					RaycastHit info;
					if (buttonCollider.Raycast (InterfaceCamera.orthoCamera.ScreenPointToRay (Input.touches [i].position), out info, SCREEN_RAY_LENGTH)) {
						touchId = Input.touches [i].fingerId;
						StartTouch (Input.touches [i].position);
						break;
					}
				}
			}
			if (Input.GetMouseButtonDown (0)) {
				RaycastHit info;
				if (buttonCollider.Raycast (InterfaceCamera.orthoCamera.ScreenPointToRay (Input.mousePosition), out info, SCREEN_RAY_LENGTH)) {
					StartTouch (Input.mousePosition);
				}
			}
		} else {
			if (touchId >= 0) { // use touch
			 	if (touchId < Input.touchCount) {
					UpdateTouch(Input.touches [touchId].position);
				} else {
					EndTouch(currentPosition);
					touchId = -1;
				}
				if (Input.touches [touchId].phase == TouchPhase.Ended || Input.touches [touchId].phase == TouchPhase.Canceled) {
					EndTouch(Input.mousePosition);
					touchId = -1;
				}
			} else { // use mouse
				if (Input.GetMouseButton (0)) {
					UpdateTouch(Input.mousePosition);
				} else {
					EndTouch(Input.mousePosition);
				}
			}
		}
		MoveObject ();
	}
	#if UNITY_EDITOR
	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(transform.position, size);
	}
	#endif
}
