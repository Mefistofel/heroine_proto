using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InterfaceButton : MonoBehaviour {
	const float SCREEN_RAY_LENGTH = 50f;

	public MonoBehaviour owner = null;
	public string controlledMethodName = "";
	public int controlledMethodParameter = 0;

	public Vector3 size = Vector3.one;
	public Vector3 center = Vector3.zero;
	BoxCollider buttonCollider;
	int touchId = -1;
	bool isMouseClicked = false;

	public void TryToFindOwner() {
		if (owner != null) {
			return;
		}
		if (transform.parent != null && transform.parent.gameObject.GetComponent<Interface>() != null) {
			owner = transform.parent.gameObject.GetComponent<Interface>();
			Debug.Log("owner " + owner.name + " finded");
		} else if (transform.parent != null && transform.parent.parent != null && transform.parent.parent.gameObject.GetComponent<Interface>() != null) {
			owner = transform.parent.parent.gameObject.GetComponent<Interface>();
			Debug.Log("owner " + owner.name + " finded");
		}
	}

	protected void Register() {
		Debug.Log (this.GetType().Name);
	}

	public string[] GetOwnerMethodNames() {
		if (owner != null) {
			List<string> methodNames = new List<string>();
			methodNames.Add("");
			var methods = owner.GetType().GetMethods();
			foreach(var method in methods) {
				if (method.GetParameters().Length == 0 && method.DeclaringType == owner.GetType()) { // methods without parameters
					methodNames.Add(method.Name);
				}
				if (method.GetParameters().Length == 1 && method.GetParameters()[0].ParameterType == typeof(int) && method.DeclaringType == owner.GetType()) { // methods without parameters
					methodNames.Add(method.Name);
				}
			}
			return methodNames.ToArray();
		}
		return new string[]{"none"};
	}

	public void SetMethodName(string newMethodName) {
		controlledMethodName = newMethodName;
	}

	void Start () {
		buttonCollider = GetComponent<BoxCollider>();
		if (buttonCollider == null) {
			buttonCollider = gameObject.AddComponent<BoxCollider>();		
		}
		buttonCollider.size = size;
		buttonCollider.center = center;
	}

	void InvokeAction() {
		if (owner != null) {
			var method = owner.GetType ().GetMethod (controlledMethodName);
			if (method != null) {
				if (method.GetParameters().Length == 0) {
					method.Invoke (owner, null); // no params
				}
				if (method.GetParameters().Length == 1 && method.GetParameters()[0].ParameterType == typeof(int)) {
					method.Invoke (owner, new object[]{controlledMethodParameter}); // int param
				}
			} else {
				if (controlledMethodName == "") {
					Debug.LogWarning ("Interface button " + name + " action not setted or does not exist in owner object");
				} else {
					Debug.LogWarning ("Interface button " + name + " action " + controlledMethodName + " does not exist in owner object");
				}
			}
		} else {
			Debug.LogWarning ("Interface button " + name + " does't have action(owner is null)");
		}
	}

	void Update () {
		for(int i = 0; i < Input.touches.Length; i++) {
			if (Input.touches[i].phase == TouchPhase.Began) {
				RaycastHit info;
				if (buttonCollider.Raycast(InterfaceCamera.orthoCamera.ScreenPointToRay(Input.touches[i].position), out info, SCREEN_RAY_LENGTH)){
					touchId = Input.touches[i].fingerId;
					break;
				}
			}
		}
		if (touchId >= 0) {
			if (Input.touches[touchId].phase == TouchPhase.Ended || Input.touches[touchId].phase == TouchPhase.Canceled) {
				RaycastHit info;
				if (buttonCollider.Raycast(InterfaceCamera.orthoCamera.ScreenPointToRay(Input.touches[touchId].position), out info, SCREEN_RAY_LENGTH)){
					if (touchId == Input.touches[touchId].fingerId) {
						InvokeAction();
					}
					touchId = -1;
				}
			}
		}
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit info;
			if (buttonCollider.Raycast(InterfaceCamera.orthoCamera.ScreenPointToRay(Input.mousePosition), out info, SCREEN_RAY_LENGTH)){
				isMouseClicked = true;
			}
		}
		if (Input.GetMouseButtonUp(0)) {
			RaycastHit info;
			if (buttonCollider.Raycast(InterfaceCamera.orthoCamera.ScreenPointToRay(Input.mousePosition), out info, SCREEN_RAY_LENGTH) && isMouseClicked){
				InvokeAction();
			}
			isMouseClicked = false;
		}
	}
	#if UNITY_EDITOR
	void OnDrawGizmos() {
		Gizmos.color = Color.green;
		Gizmos.DrawWireCube(transform.position + center, size);
	}
	#endif
}
