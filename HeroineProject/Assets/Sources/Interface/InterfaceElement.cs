﻿using UnityEngine;
using System.Collections;

public enum ElementLink {
	Absolute,
	// TODO
	Top,
	Bottom,
	Left,
	Right,
	// TODO
	TopLeft,
	TopRight,
	BottomLeft,
	BottomRight,
	// TODO
	Proportional 
}

public class InterfaceElement : MonoBehaviour {
	[SerializeField] Vector3 pivot;
	[SerializeField] public ElementLink link;
	const float baseSize = 6;
	int screenWidth;
	int screenHeight;
	Vector3 absolutePosition;

	void Start () {
		absolutePosition = transform.localPosition;
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		UpdateObjectPosition ();
	}

	void UpdateObjectPosition(){
		if (InterfaceCamera.orthoCamera == null) {
			#if UNITY_EDITOR
			Debug.LogWarning ("need to set up interface camera");
			#endif
			return;
		}
		Vector2 aspect = new Vector2((float)InterfaceCamera.orthoCamera.orthographicSize / (float)screenHeight * (float)screenWidth, InterfaceCamera.orthoCamera.orthographicSize);
		float aspectRatio = (float)screenWidth / (float)screenHeight;
		if (aspectRatio < 1f) {
			transform.localScale = Vector3.one * (aspectRatio);
		} else {
			aspectRatio = 1f;
			transform.localScale = Vector3.one * (aspectRatio);
		}
		switch (link) {
		case ElementLink.Absolute:
			transform.localPosition = new Vector3(absolutePosition.x * aspectRatio, absolutePosition.y * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.Top:
			transform.localPosition = new Vector3(absolutePosition.x * aspectRatio, aspect.y + (-baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.Bottom:
			transform.localPosition = new Vector3(absolutePosition.x * aspectRatio,-aspect.y + ( baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.Left:
			transform.localPosition = new Vector3(-aspect.x + (absolutePosition.x + baseSize) * aspectRatio, absolutePosition.y * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.Right:
			transform.localPosition = new Vector3( aspect.x + (absolutePosition.x - baseSize) * aspectRatio, absolutePosition.y * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.TopLeft:
			transform.localPosition = new Vector3(-aspect.x + (absolutePosition.x + baseSize) * aspectRatio, aspect.y + (-baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.TopRight:
			transform.localPosition = new Vector3( aspect.x + (absolutePosition.x - baseSize) * aspectRatio, aspect.y + (-baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.BottomLeft:
			transform.localPosition = new Vector3(-aspect.x + (absolutePosition.x + baseSize) * aspectRatio,-aspect.y + ( baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.BottomRight:
			transform.localPosition = new Vector3( aspect.x + (absolutePosition.x - baseSize) * aspectRatio,-aspect.y + ( baseSize + absolutePosition.y) * aspectRatio, absolutePosition.z);
			break;
		case ElementLink.Proportional: // scale sides proportional
			transform.localPosition = new Vector3(absolutePosition.x / baseSize * aspect.x, absolutePosition.y / baseSize * aspect.y, absolutePosition.z);
			break;
		}
	}

	void Update () {
		// TODO События должны собираться не для каждого элемента, а централизованно
		if (screenWidth != Screen.width || screenHeight != Screen.height) {
			screenWidth = Screen.width;
			screenHeight = Screen.height;
			UpdateObjectPosition();
		}
	}
}
