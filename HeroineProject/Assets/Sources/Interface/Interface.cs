﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using System.Collections.Generic;

public class Interface : MonoBehaviour {
	public const int INTERFACE_LAYER = 5;
	public int interfaceLevel = 0;
	static Dictionary<System.Type, Interface> hierarchy = new Dictionary<System.Type, Interface>();
	static Interface[] stack = new Interface[10];

	public Rect QuadRect(float x, float y, float w = 1f, float h = 1f) {
		if (Screen.width > Screen.height) {
			float xShift = (float)(Screen.width - Screen.height) / 2f;
			float basePixels = (float)Screen.height / 10f;
			return new Rect (xShift + x * basePixels, y * basePixels, w * basePixels, h * basePixels);
		} else {
			float yShift = (float)(Screen.height - Screen.width) / 2f;
			float basePixels = (float)Screen.width / 10f;
			return new Rect (x * basePixels, yShift + y * basePixels, w * basePixels, h * basePixels);
		}
	}

	public static T Create<T>(int interfaceLevel = 0) where T : Interface{
		if (hierarchy.ContainsKey (typeof(T))) {
			return (T)hierarchy[typeof(T)];
		}
		GameObject interfaceObject = new GameObject();
//		interfaceObject.transform.parent = MainCamera.mainCamera.transform;
		interfaceObject.transform.position = Vector3.zero;
		interfaceObject.name = typeof(T).Name;

		T interfaceComponent = interfaceObject.AddComponent<T>();
		if (!hierarchy.ContainsKey (typeof(T))) {
			hierarchy.Add (typeof(T), interfaceComponent);
		}
		interfaceComponent.interfaceLevel = interfaceLevel;
		interfaceComponent.Init ();
		interfaceObject.SetActive (false);
		return (T)interfaceComponent;
	}

	public static void Show<T>(int needLevel = -1) where T : Interface {
		if (hierarchy.ContainsKey (typeof(T))) {
			int level = needLevel;
			if (level < 0) {
				level = hierarchy [typeof(T)].interfaceLevel;
			}
			if (stack[level] == hierarchy [typeof(T)]) {
				return;
			}
			HideLevel(level);
			ClearLevelStack(level);
			stack[level] = hierarchy [typeof(T)];
			stack[level].OnShow ();
		} else {
			Create<T>();
			Interface.Show<T>();
		}
	}

	public static void HideLevel(int fromLevel = 0){
		for (int i = stack.Length - 1; i >= fromLevel; i--) {
			if (stack[i] != null) {
				stack[i].OnHide();
			}
		}
	}

	public static void ClearLevelStack(int fromLevel = 1){
		for (int i = fromLevel + 1; i < stack.Length; i++) {
			stack[i] = null;
		}
	}
	
	public static void UpdateInterface() {
		if (hierarchy != null) {
			foreach(Interface i in hierarchy.Values) {
				i.UpdateState();
			}
		}
	}

	protected virtual void Awake() {
		if (!hierarchy.ContainsKey (this.GetType())) {
			hierarchy.Add(this.GetType(), this);
		}
	}

	protected virtual void Init() {
		Debug.Log(this.GetType().Name);
	}

	protected virtual void OnShow() {
		this.gameObject.SetActive (true);
	}

	protected virtual void OnHide() {
		this.gameObject.SetActive (false);
	}

	protected virtual void OnDestroy() {
		hierarchy.Remove (this.GetType());
	}
	
	public virtual void UpdateState(){
		
	}
}
