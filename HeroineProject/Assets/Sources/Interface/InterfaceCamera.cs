using UnityEngine;
using System.Collections;

public class InterfaceCamera : MonoBehaviour {
	
	public static InterfaceCamera instance;
	
	public const int InterfaceLayer = 5;
	public static Vector2 CameraSize = new Vector2(Screen.width, Screen.height) * (7.68f / Screen.height);
	private static float screenSize = Screen.height;

	float cameraHeight = 6f;
	int currentWidth = 0;
	int currentHeight = 0;
	readonly Vector3 INTERFACE_CAMERA_POSITION = new Vector3(0, 0, -50);
	public Camera interfaceCamera;

	public static Camera orthoCamera {
		get {
			if (instance == null) {
				Debug.LogWarning("InterfaceCamera not initiated");
				return null;
			}
			return instance.interfaceCamera;
		}
	}

	void SetCameraParameters() {
		interfaceCamera.depth = 10;
		interfaceCamera.isOrthoGraphic = true;
		interfaceCamera.orthographicSize = cameraHeight;
		interfaceCamera.clearFlags = CameraClearFlags.Nothing;
		interfaceCamera.nearClipPlane = 0.2f;
		interfaceCamera.farClipPlane = 100;
		interfaceCamera.cullingMask = 1 << InterfaceLayer;
	}

	void Awake () {
		if (instance != null) {
			Destroy(gameObject);
			return;
		}
		instance = this;
		interfaceCamera = GetComponentInChildren<Camera> ();
		if (interfaceCamera == null) {
			GameObject interfaceCameraObject = new GameObject("InterfaceCamera");
			interfaceCameraObject.transform.parent = transform;
			interfaceCameraObject.transform.position = INTERFACE_CAMERA_POSITION;
			interfaceCamera = interfaceCameraObject.AddComponent<Camera>();
		}
		SetCameraParameters ();
		cameraHeight = interfaceCamera.orthographicSize;
		screenSize   = Screen.height;
		DontDestroyOnLoad (gameObject);
		UpdateAspect();
	}
	
	public static void UpdateAspect() {
		if (instance != null) {
			CameraSize = new Vector2(Screen.width, Screen.height) * (instance.cameraHeight / Screen.height);
			Interface.UpdateInterface();
		}
	}

	void Update() {
		if (currentWidth != Screen.width || currentHeight != Screen.height) {
			currentWidth = Screen.width;
			currentHeight = Screen.height;
			UpdateAspect();
		}
	}

	public Vector2 FindCoords(Vector2 touchPosition){
		return (cameraHeight * 2f) / screenSize * touchPosition - CameraSize;
	}
}
