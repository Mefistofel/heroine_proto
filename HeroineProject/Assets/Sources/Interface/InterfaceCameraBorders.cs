﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class InterfaceCameraBorders : MonoBehaviour {

	public Camera interfaceCamera;

	void Start () {
		if (interfaceCamera == null) {
			#if UNITY_EDITOR
			Debug.LogWarning ("need to set up source camera in InterfaceCameraBorders component on " + gameObject.name + " gameObject");
			#endif
		} else {
			#if UNITY_EDITOR
			if (!interfaceCamera.isOrthoGraphic) {
				Debug.LogWarning ("There must be ortho camera in source in InterfaceCameraBorders component on " + gameObject.name + " gameObject");
			}
			#endif
		}
	}

	#if UNITY_EDITOR
	void OnDrawGizmos() {
		if (interfaceCamera != null) {
			float sizeY = interfaceCamera.orthographicSize;
			float sizeX = sizeY / (float)Screen.height * (float)Screen.width;
			float min = (sizeX > sizeY)? sizeY : sizeX;
			Gizmos.color = Color.blue;
			Gizmos.DrawLine (new Vector3 ( sizeX,  sizeY) + transform.localPosition, new Vector3 ( sizeX, -sizeY) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 ( sizeX, -sizeY) + transform.localPosition, new Vector3 (-sizeX, -sizeY) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 (-sizeX, -sizeY) + transform.localPosition, new Vector3 (-sizeX,  sizeY) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 (-sizeX,  sizeY) + transform.localPosition, new Vector3 ( sizeX,  sizeY) + transform.localPosition);
			Gizmos.color = Color.green;
			Gizmos.DrawLine (new Vector3 ( min,  min) + transform.localPosition, new Vector3 ( min, -min) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 ( min, -min) + transform.localPosition, new Vector3 (-min, -min) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 (-min, -min) + transform.localPosition, new Vector3 (-min,  min) + transform.localPosition);
			Gizmos.DrawLine (new Vector3 (-min,  min) + transform.localPosition, new Vector3 ( min,  min) + transform.localPosition);
		}
	}
	#endif
}
