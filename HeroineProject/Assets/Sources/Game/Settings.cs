﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class Settings {
	static Settings instance;
	const string DEFAULT_SETTINGS_FILE_NAME = "settings.xml";

	[XmlAttribute ("sound")]
	public bool soundEnabled = true;
	[XmlAttribute ("music")]
	public bool musicEnabled = true;

	public static bool SoundEnabled{
		get {return instance.soundEnabled;}
		set {instance.soundEnabled = value;}
	}
	public static bool MusicEnabled{
		get {return instance.musicEnabled;}
		set {instance.musicEnabled = value;}
	}

	static Settings () {
		Load();
	}
	
	public Settings () { }

	public static void Load (string fileName = DEFAULT_SETTINGS_FILE_NAME) {
		if (File.Exists(Application.persistentDataPath + "/" + fileName)) {
			try {
				XmlSerializer serialReader = new XmlSerializer (typeof(Settings));
				Stream reader = new FileStream (Application.persistentDataPath + "/" + fileName, FileMode.Open);
				instance = serialReader.Deserialize (reader) as Settings;
				reader.Close ();
			}
			catch {
				instance = new Settings();
			}
		} else {
			instance = new Settings();
		}
	}

	public static void Save (string fileName = DEFAULT_SETTINGS_FILE_NAME) {
		XmlSerializer serialWrite = new XmlSerializer(typeof(Settings));
		XmlTextWriter writer = new XmlTextWriter (Application.persistentDataPath + "/" + fileName, System.Text.UTF8Encoding.UTF8);
		serialWrite.Serialize (writer, instance);
		writer.Close();
	}
}
