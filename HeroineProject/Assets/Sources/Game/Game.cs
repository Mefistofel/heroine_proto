using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour {
	static Game instance;

	[SerializeField]MapConstructor constructor = null;
	[SerializeField]TextAsset currentLevel = null;
	[SerializeField]TextAsset questsInfo = null;


	Level level = null;
	HeroMarker hero = null;
	
	Camera viewCamera = null;

	void Start () {
		instance = this;
		if (constructor != null) {
			Map.instance = new Map(constructor.GetCells());
			Destroy(constructor.gameObject);
		}
		if (currentLevel != null) {
			Map.instance = Map.LoadFromAsset(currentLevel);
		}
		if (Map.instance == null) {
			Map.instance = new Map (null);
		}
		level = gameObject.AddComponent<Level> ();
		level.Init (Map.instance);
		hero = HeroMarker.Instance;// Utils.Create<HeroMarker> ();
		hero.SetPosition (Level.GetHexPosition(Player.info.coordinates));
		hero.GetComponent<HumanController> ().SetSkin (Player.info.skin);
		viewCamera = CameraController.viewCamera;
		// quest

//		QuestNode startNode = new QuestNode ();
//		startNode.id = "start";
//		startNode.text = "test node text";
//		startNode.answers = new QuestAnswer[]
//		{
//			new QuestAnswer("ok"),
//			new QuestAnswer("cansel")
//		};

//		DialogController.ShowDialog (startNode);
//		QuestLog.Instance.nodes.Add (startNode);
//		quests = new QuestLog();
//		quests.nodes.Add (startNode);
		if (questsInfo != null) {
			QuestLog.LoadFromAsset(questsInfo);
		}
	}
	
	void OnHeroMoveEnded() { // start action
		Cell cell = Map.instance [Player.info.coordinates];
		if (cell.enemy != null) {
			Battle.StartBattle(
				cell.enemy,
				(result) => {
				cell.enemy = null;
				Map.instance [Player.info.coordinates]= cell;
				Debug.Log("battle result " + result.isWin);
			});
		}
		if (cell.nodeId != null && cell.nodeId != "") {
			Debug.Log(cell.nodeId);
			DialogController.ShowDialog (QuestLog.quests[cell.nodeId]);
		}
	}

	void OnMapClick(Coord clickPosition) {
		if (clickPosition == Player.info.coordinates) { // click on hero
			Debug.Log ("I am hero!");
		} else {
			if (Map.instance [clickPosition].isCanMove) {
				if (!hero.IsMoved) { // hero stay
					Vector3[] path = level.FindPath(Player.info.coordinates, clickPosition);
					hero.MoveByPath (path, OnHeroMoveEnded);
					Player.info.coordinates = clickPosition;
				} else { // stop hero on nearest coord and refind path
					Vector3 startPoint = hero.transform.localPosition;
					Coord currentPosition = Level.GetPositionHexCoord(startPoint);
					Vector3[] path = level.FindPath(currentPosition, clickPosition);
					path[0] = startPoint;
					hero.MoveByPath (path, OnHeroMoveEnded);
					Player.info.coordinates = clickPosition;
				}
			}
		}
	}

	void CheckClick(Vector3 position) {
		if (viewCamera != null) {
			Ray mouseRay = viewCamera.ScreenPointToRay (position);
			Plane floorPlane = new Plane (Vector3.up, Vector3.zero); // floor
			float distance = 0;
			if (!Physics.Raycast(mouseRay)) {
				if (floorPlane.Raycast (mouseRay, out distance)) {
					Vector3 contactPoint = mouseRay.GetPoint (distance);
					Coord clickPosition = Level.GetPositionHexCoord (contactPoint);
					OnMapClick (clickPosition);
				}
			}
		}
	}

	void Update() {
		// check click ray on zero level
		if (Input.GetKey (KeyCode.Escape)) {
			Application.Quit();
		}
		if (Input.GetMouseButtonUp (0)) {
			CheckClick(Input.mousePosition);
		}
		if (Input.touchCount > 0 && (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)) {
			CheckClick(Input.touches[0].position);
		}

	}
}