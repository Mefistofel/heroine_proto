﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player {
	public HumanSkinInfo skin = HumanSkinInfo.RandomSkin();
	public static Player info;

	public string name = "Stranger";
	public Coord coordinates = new Coord(10, 10);

	public float health = 100;
	public float maximumHealth = 100;

	public float level = 1;

	// parameters
	public int strength = 1;
	public int agility = 1;
	public int luck = 1; // ?
	public int stamina = 1; // from str?

	// inventory
	public int swordLevel = 0;
	public int shieldLevel = 0;

	public bool[] artifacts = null;
	public int healthPotion = 0;
	public int powerPotion = 0;
	public int luckPotion = 0;
	public int staminaPotion = 0;

	static Player() {
		info = new Player ();
	}
}
