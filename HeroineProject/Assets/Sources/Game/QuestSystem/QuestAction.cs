﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class QuestAction { // action on quest node process

	[System.Serializable]
	public enum Type {
		[XmlEnum("tv")] SetTriggerValue,
		[XmlEnum("it")] IncreaseTrigger,
		[XmlEnum("dt")] DecreaseTrigger,
//		[XmlEnum("gm")] GiveMoney,
//		[XmlEnum("gi")] GiveItem,
//		[XmlEnum("tm")] TakeMoney,
//		[XmlEnum("ti")] TakeItem
	}

	[XmlAttribute ("type")]
	public Type type = Type.SetTriggerValue;

	[XmlAttribute ("name")]
	public string triggerName = ""; // name of trigger or item

	[XmlAttribute ("value")]
	public int subjectValue = 0; // new value of action - how many moneys give, or new trigger value


	public QuestAction() {}

	public QuestAction(Type newType, string name, int value = 0) {
		type = newType;
		triggerName = name;
		subjectValue = value;
	}

	public void Invoke() {
		Trigger trigger = WorldState.GetTrigger (triggerName);
		if (trigger == null) {
			trigger = WorldState.AddTrigger (triggerName);
		}
		Debug.Log("Action: " + type.ToString() + ": " + trigger.name + " " + trigger.value);
		switch (type) {
		case Type.SetTriggerValue:
			if (trigger != null) {
				trigger.value = subjectValue;
				return;
			}
			break;
		case Type.IncreaseTrigger:
			if (trigger != null) {
				trigger.value += subjectValue;
				return;
			}
			break;
		case Type.DecreaseTrigger:
			if (trigger != null) {
				trigger.value -= subjectValue;
				return;
			}
			break;
		}
		Debug.LogWarning ("No trigger in Action");
	}
}