﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class WorldState {
	static WorldState state = null;
	const string DEFAULT_STATE_FILE_NAME = "state.xml";

	[XmlArray("triggers")]
	public List<Trigger> triggers = new List<Trigger>();

	public static Trigger GetTrigger(string name) {
		if (state != null && state.triggers != null) {
			foreach (var trigger in state.triggers) {
				if (trigger != null && trigger.name == name) {
					return trigger;
				}
			}
			Debug.Log("No such triggers");
			return null;
		}
		Debug.Log("State not initialized");
		return null;
	}

	public static Trigger AddTrigger(string name, int value = 0) {
		return AddTrigger(new Trigger(name, value));
	}
	public static Trigger AddTrigger(Trigger newTrigger) {
		if (state != null && state.triggers != null) {
			state.triggers.Add(newTrigger);
			Debug.Log("Trigger added: " + newTrigger.name);
			return newTrigger;
		}
		Debug.Log("GameState not initiated");
		return null;
	}

	static WorldState() {
		Load();
	}

	public WorldState() {}
	
	public static void Load (string fileName = DEFAULT_STATE_FILE_NAME) {
		if (File.Exists(Application.persistentDataPath + "/" + fileName)) {
			try {
				XmlSerializer serialReader = new XmlSerializer (typeof(WorldState));
				Stream reader = new FileStream (Application.persistentDataPath + "/" + fileName, FileMode.Open);
				state = serialReader.Deserialize (reader) as WorldState;
				reader.Close ();
			}
			catch {
				state = new WorldState();
			}
		} else {
			state = new WorldState();
		}
	}

	public static void Save (string fileName = DEFAULT_STATE_FILE_NAME) {
		XmlSerializer serialWrite = new XmlSerializer(typeof(WorldState));
		XmlTextWriter writer = new XmlTextWriter (Application.persistentDataPath + "/" + fileName, System.Text.UTF8Encoding.UTF8);
		serialWrite.Serialize (writer, state);
		writer.Close();
	}
}