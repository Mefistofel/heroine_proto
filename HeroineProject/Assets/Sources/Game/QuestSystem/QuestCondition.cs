using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
[XmlRoot ("val")]
public class ConditionElement {

	[System.Serializable]
	public enum Type {
		[XmlEnum("const")] Constanta,
		[XmlEnum("trggr")] Trigger
	}
	
	[XmlAttribute ("type")]
	public Type type = Type.Constanta;
	[XmlAttribute ("name")]
	public string name = "";
	[XmlAttribute ("val")]
	public int value = 0;

	public ConditionElement() {}

	public int Value {
		get {
			switch(type) {
			default:
			case Type.Constanta:
				return this.value;
			case Type.Trigger:
				Trigger trigger = WorldState.GetTrigger(name);
				if (trigger != null) {
					return trigger.value;
				}
				Debug.LogWarning("Condition value contain empty Token " + name);
				return value;
			}
		}
	}

	public ConditionElement(Type newType, string newName, int newValue = 0) {
		type = newType;
		name = newName;
		value = newValue;
	}
	
	public ConditionElement(int newValue) { // constant
		type = Type.Constanta;
		value = newValue;
	}

	public static ConditionElement Trigger(string triggerName, int newValue = 0) {
		return new ConditionElement (Type.Trigger, triggerName, newValue);
	}
}

[System.Serializable]
[XmlRoot ("condition")]
public class QuestCondition {

	[System.Serializable]
	public enum ConditionType {
		[XmlEnum("equal")] Equal,
		[XmlEnum("less")]  Less,
		[XmlEnum("more")]  More,
		[XmlEnum("lesse")] LessOrEqual,
		[XmlEnum("moree")] MoreOrEqual,
		[XmlEnum("noneq")] NoneEqual,
		[XmlEnum("or")]    Or, // 0 - false 1 - true
		[XmlEnum("and")]   And,
		[XmlEnum("xor")]   Xor
	}

	[XmlElement("A")]
	public ConditionElement valueA = null;
	[XmlElement("B")]
	public ConditionElement valueB = null;
	[XmlAttribute ("con")]
	public ConditionType condition = ConditionType.Equal;
	
	
	public QuestCondition() {}
	
	public QuestCondition(ConditionType newCondition, ConditionElement newValueA, ConditionElement newValueB) {
		condition = newCondition;
		valueA = newValueA;
		valueB = newValueB;
	}
	
	public QuestCondition(ConditionType newCondition, ConditionElement trigger, int constantValue = 0) {
		condition = newCondition;
		valueA = trigger;
		valueB = new ConditionElement(constantValue);
	}

	public bool Check () {
		if (valueA == null || valueB == null) {
			Debug.LogWarning("Condition contain empty value");
			return false;
		}
		switch(condition){
		case ConditionType.Equal:
			return (valueA.value == valueB.value);
		case ConditionType.More:
			return (valueA.value > valueB.value);
		case ConditionType.Less:
			return (valueA.value < valueB.value);
		case ConditionType.MoreOrEqual:
			return (valueA.value >= valueB.value);
		case ConditionType.LessOrEqual:
			return (valueA.value <= valueB.value);
		case ConditionType.Or:
			return ((valueA.value > 0) || (valueB.value > 0));
		case ConditionType.And:
			return ((valueA.value > 0) && (valueB.value > 0));
		case ConditionType.Xor:
			return ((valueA.value > 0) != (valueB.value > 0));
		}
		return false;
	}
}   