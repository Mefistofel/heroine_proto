﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class Trigger {
	[XmlAttribute ("name")]
	public string name;
	[XmlAttribute ("value")]
	public int value = 0;

	public Trigger(string newName, int newValue = 0) {
		name = newName;
		value = newValue;
	}

	public Trigger() {
		name = "";
	}
}