﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class QuestNode{
	
	[XmlAttribute ("id")]
	public string id = ""; // id of node for search and selection

	[XmlAttribute ("text")]
	public string text = ""; // main node text/text tag(for translate)
	
	[XmlArray ("conditions")]
	public QuestCondition[] showConditions = null; // show node only if all conditions is success

	[XmlArray ("actions")]
	public QuestAction[] actions = null; // actions on activate

	[XmlArray ("answers")]
	public QuestAnswer[] answers = null; // optional

	public bool CheckConditions () {
		if (showConditions != null) {
			foreach(var condition in showConditions) {
				if (condition != null && !condition.Check()) {
					return false;
				}
			}
		}
		return true;
	}
}
