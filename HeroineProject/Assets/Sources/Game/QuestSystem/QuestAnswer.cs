﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class QuestAnswer {

	[XmlAttribute ("answer")]
	public string answer = ""; // answer text/text tag(for translate)
	
	[XmlAttribute ("next")]
	public string nextNodeId = ""; // next node selected after that answer

//	[XmlArray ("actions")]
	public QuestAction[] actions = null; // optional - actions after answer selection
	
//	[XmlArray ("conditions")]
	public QuestCondition[] conditions = null; // optional - conditions of showing

	public QuestAnswer() {}

	public QuestAnswer(string answerText, string nextNode, QuestAction[] answerActions = null, QuestCondition[] answerConditions = null) {
		answer = answerText;
		actions = answerActions;
		nextNodeId = nextNode;
		conditions = answerConditions;
	}
	public QuestAnswer(string answerText, QuestAction[] answerActions = null, QuestCondition[] answerConditions = null)
	{
		answer = answerText;
		actions = answerActions;
		conditions = answerConditions;
	}
}
