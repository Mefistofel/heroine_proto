﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum WeaponType {
	weapon_0,
	weapon_1,
	weapon_2,
	weapon_3,
	weapon_4,
	weapon_5,
	weapon_6,
	weapon_7,
	weapon_8,
	weapon_9,
	weapon_10,
	weapon_11,
	weapon_12,
}
[System.Serializable]
public enum ShieldType {
	shield_0,
	shield_1,
	shield_2,
	shield_3,
	shield_4,
	shield_5,
	shield_6,
	shield_7,
	shield_8,
	shield_9,
}
[System.Serializable]
public enum ArmorType {
	armor_0,
	armor_1,
	armor_2,
	armor_3,
	armor_4,
	armor_5,
}
[System.Serializable]
public enum HelmetType {
	helm_0,
	helm_1,
	helm_2,
	helm_3,
	helm_4,
	helm_5,
	helm_6,
}

[System.Serializable]
public class HumanSkinInfo {
	public WeaponType weapon = WeaponType.weapon_0;
	public ShieldType shield = ShieldType.shield_0;
	public ArmorType  armor  = ArmorType.armor_0;
	public HelmetType helmet = HelmetType.helm_0;
	public static HumanSkinInfo RandomSkin() {
		return new HumanSkinInfo () {
			weapon = (WeaponType)Random.Range(0, WeaponType.GetNames(typeof(WeaponType)).Length - 1),
			shield = (ShieldType)Random.Range(0, ShieldType.GetNames(typeof(ShieldType)).Length - 1),
			armor  = (ArmorType )Random.Range(0, ArmorType.GetNames(typeof(ArmorType)).Length - 1),
			helmet = (HelmetType)Random.Range(0, HelmetType.GetNames(typeof(HelmetType)).Length - 1)
		};
	}
}

public class HumanController : MonoBehaviour {
	[SerializeField]public HumanSkinInfo skin = new HumanSkinInfo();// Set from editor
	[SerializeField]GameObject weaponController = null;// Set from editor
	[SerializeField]GameObject shieldController = null;// Set from editor
	[SerializeField]GameObject armorController = null; // Set from editor
	[SerializeField]GameObject helmetController = null;// Set from editor

	[ContextMenu("Reassembly")]
	public void EquipCurrentSkin() {
		weaponController = ReplaceDetail (weaponController, "Prefabs/Human/Weapons/" + skin.weapon.ToString ());
		shieldController = ReplaceDetail (shieldController, "Prefabs/Human/Shields/" + skin.shield.ToString ());
		armorController  = ReplaceDetail (armorController , "Prefabs/Human/Armors/"  + skin.armor.ToString ());
		helmetController = ReplaceDetail (helmetController, "Prefabs/Human/Helmets/" + skin.helmet.ToString ());
	}

	void Start() {
//		EquipCurrentSkin();
	}

	public void SetSkin(HumanSkinInfo info) {
		skin = info;
		EquipCurrentSkin();
	}

	GameObject ReplaceDetail(GameObject rootController, string prefabName) {
		if (rootController != null) {
			GameObject newObject = (GameObject)Instantiate (Resources.Load (prefabName));
			if (newObject != null) {
				newObject.transform.parent = rootController.transform.parent;
				newObject.transform.localPosition = rootController.transform.localPosition;
				newObject.transform.localEulerAngles = rootController.transform.localEulerAngles;
				newObject.transform.localScale = rootController.transform.localScale;
				Destroy(rootController);
				return newObject;
			}
		}
		return rootController;
	}


}
