﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public static CameraController instance = null;
	public Transform selfTransform = null;

	public static Camera viewCamera = null;

	void Awake () {
		instance = this;
		selfTransform = transform;
		viewCamera = GetComponentInChildren<Camera> ();
	}

	public static void AnimateZoom(float from, float to, float time = 0.5f) {
		Timer.Add (
			time,
		    (anim) => {
				if (viewCamera != null) {
					viewCamera.transform.localPosition = new Vector3(0, 0, to + (from - to) * (1f - anim));
				}
			},
		() => {
			if (viewCamera != null) {
				viewCamera.transform.localPosition = new Vector3(0, 0, to);
			}
			});
	}
}
