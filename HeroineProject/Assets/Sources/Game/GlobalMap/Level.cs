using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// create level image by map info
public class Level : MonoBehaviour {
	public const float cellSize = 2.2f;

	public static Vector3 shift = Vector3.zero;
	Map map = null;

	public void Init(Map newMap) {
		// create level from assets
		map = newMap;
//		shift = new Vector3 ((float)map.width / 2f, 0, (float)map.height / 2f) * -cellSize;
		for (int i = 0; i < map.width; i ++) {
			for (int j = 0; j < map.height; j ++) {
				CreateTile(i, j, map[i, j].type, map[i, j].model);
				if (map[i, j].enemy != null) {
					AddEnemy(i, j, map[i, j].enemy);
				}
			}
		}
	}

	public Vector3[] FindPath(Coord from, Coord to) {
		Coord[] coordPath = map.FindPath (to, from, 16);
		if (coordPath != null && coordPath.Length > 1) {
			Vector3[] path = new Vector3[coordPath.Length];
			for (int i = 0; i < coordPath.Length; i++) {
				path[i] = GetHexPosition(coordPath[i]);
			}
			return CreateSmoothWay(path);
		}
		return null;
	}

	public static Vector3 GetHexPosition(float x, float y) {
		return (new Vector3(x - y * 0.5f, 0, y) * cellSize + shift);
	}
	
	public static Vector3 GetHexPosition(Coord pos) {
		return GetHexPosition(pos.x, pos.y);
	}

	public static Coord GetPositionHexCoord(Vector3 point) {
		//new Vector3(x - y * 0.5f, 0, y) * cellSize + shift
		point = (point - shift) * (1f / cellSize);
		int y = (int)Mathf.Round (point.z);
		int x = (int)Mathf.Round (point.x + point.z * 0.5f);
		return new Coord (x, y);
	}

	void AddEnemy(float i, float j, Enemy enemyInfo) {
		GameObject enemy = null;
		switch (enemyInfo.type) {
		case Enemy.Type.raider:
			break;
		}
		enemy = (GameObject)Instantiate(Resources.Load("Prefabs/Enemy"));
		if (enemy != null) {
			enemy.transform.parent = transform;
			enemy.transform.localPosition = GetHexPosition(i, j);
		}
	}

	void CreateTile (float i, float j, CellType type, CellModel model) {
		GameObject tile = null;
		switch (type) {
		case CellType.Grass:
			tile = (GameObject)Instantiate(Resources.Load("Prefabs/Ground/dummyGrass"));
			break;
		case CellType.Desert:
			tile = (GameObject)Instantiate(Resources.Load("Prefabs/Ground/dummyDesert"));
			break;
		case CellType.Stone:
			tile = (GameObject)Instantiate(Resources.Load("Prefabs/Ground/dummyStone"));
			break;
		case CellType.Water:
			tile = (GameObject)Instantiate(Resources.Load("Prefabs/Ground/dummyWater"));
			break;
		default:
			break;
		}
		if (tile != null) {
			tile.transform.parent = transform;
			tile.transform.localPosition = GetHexPosition(i, j);
		}
		GameObject modelMesh = null;
		switch (model) {
		case CellModel.Rock:
			modelMesh = (GameObject)Instantiate(Resources.Load("Prefabs/Enviroment/dummyMountain"));
			break;
		case CellModel.Ruins:
			modelMesh = (GameObject)Instantiate(Resources.Load("Prefabs/Enviroment/dummyRuins"));
			break;
		case CellModel.Forest:
			modelMesh = (GameObject)Instantiate(Resources.Load("Prefabs/Enviroment/dummyForest"));
			break;
		case CellModel.City:
			modelMesh = (GameObject)Instantiate(Resources.Load("Prefabs/Enviroment/dummyCity"));
			break;
		default:
			break;
		}
		if (modelMesh != null) {
			modelMesh.transform.parent = transform;
			modelMesh.transform.localPosition = GetHexPosition(i, j);
		}
	}

	void Start () {

	}

	void Update () {
	}

	static Vector3[] CreateSmoothWay (Vector3[] way) {
		const int smoothEdgeCount = 12;
		if (way == null) {
			return null;
		}
		if (way.Length <= 2) {
			return way;
		}
		Vector3[] newWay = new Vector3[(way.Length - 2) * smoothEdgeCount + 2];
		newWay [0] = way [0];
		newWay [newWay.Length - 1] = way [way.Length - 1];
		for (int i = 0; i < way.Length - 2; i++) {
			Vector3 startPoint = Vector3.Lerp (way[i], way[i + 1], 0.5f);
			Vector3 endPoint = Vector3.Lerp (way[i + 1], way[i + 2], 0.5f);
			Vector3 anglePoint = way[i + 1];
			for (int j = 0; j < smoothEdgeCount; j ++){
				newWay[1 + i * smoothEdgeCount + j] = Besie(startPoint, endPoint, anglePoint, (float)j / (float)(smoothEdgeCount) + (1f / (float)smoothEdgeCount * 0.5f));
			}
		}
		return newWay;
	}
	
	static Vector3 Besie(Vector3 start, Vector3 end, Vector3 edge, float t) {
		Vector3 a = Vector3.Lerp (start, edge, t);
		Vector3 b = Vector3.Lerp (edge, end, t);
		return Vector3.Lerp (a, b, t);
	}
}
