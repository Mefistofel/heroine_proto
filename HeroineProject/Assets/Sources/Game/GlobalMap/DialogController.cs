﻿using UnityEngine;
using System.Collections;

public class DialogController : MonoBehaviour {
	static DialogController instance = null;

	[SerializeField] TextMesh nodeText = null; // Fill from editor
	[SerializeField] TextMesh[] answerTexts = null; // Fill from editor
	[SerializeField] InterfaceButton[] answerButtons = null; // Fill from editor

	QuestNode currentNode = null;

	public static void ShowDialog(QuestNode node) {
		if (instance != null) {
			if (node != null) {
				instance.Show(node);
			} else {
				instance.Hide ();
				Debug.LogWarning("Dialog node is empty");
			}
		} else {
			Debug.LogWarning("DialogController not initialized");
		}
	}

	// Fill from editor - call from interface part
	public void OnAnswerSelect(int answerId) {
		if (currentNode.answers != null && answerId < currentNode.answers.Length && currentNode.answers[answerId] != null) {
			InvokeActions();
			if (currentNode.answers[answerId].nextNodeId != null && currentNode.answers[answerId].nextNodeId != "") {
				Show (QuestLog.quests[currentNode.answers[answerId].nextNodeId]);
			} else {
				Hide ();
			}
		}
	}
	
	// Fill from editor - call from interface part
	public void OnBackgroundSelect() {
		if (currentNode.answers == null) {
			InvokeActions();
			Hide ();
		}
	}

	void InvokeActions() {
		Debug.Log("make actions");
		if (currentNode != null && currentNode.actions != null) {
			foreach(var action in currentNode.actions) {
				if (action != null) {
					action.Invoke();
				}
			}
		} 
	}

	void Show(QuestNode node) {
		// TODO animation
		gameObject.SetActive (true);
		currentNode = node;
		if (nodeText != null) {
			nodeText.text = node.text;
		}
		int answerCount = 0;
		if (node.answers != null) {
			answerCount = node.answers.Length;
		}
		if (answerTexts != null) {
			for(int i = 0; i < answerTexts.Length; i++) {
				if (answerTexts[i] != null) {
					if (i < answerCount && node.answers[i] != null) {
						answerTexts[i].gameObject.SetActive(true);
						answerTexts[i].text = node.answers[i].answer;
					} else {
						answerTexts[i].gameObject.SetActive(false);
					}
				}
			}
		}
		if (answerButtons != null) {
			for(int i = 0; i < answerButtons.Length; i++) {
				if (answerButtons[i] != null) {
					bool isAnswerActive = (i < answerCount && node.answers[i] != null);
					answerButtons[i].gameObject.SetActive(isAnswerActive);
				}
			}
		}
	}

	void Hide() {
		// TODO animation
		gameObject.SetActive (false);
	}

	void Start () {
		instance = this;
		Hide ();
	}

	void Update () {
	
	}
}
