﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif
[System.Serializable]
public enum CellType {
	Void,
	Grass,
	Desert,
	Water,
	Stone
}
[System.Serializable]
public enum CellModel {
	None,
	Rock,
	Forest,
	City,
	Ruins,
	Door
}
[System.Serializable]
public struct Coord {
	[XmlAttribute("x")]
	public int x;
	[XmlAttribute("y")]
	public int y;

	public Coord(int newX, int newY) {
		x = newX;
		y = newY;
	}

	public static bool operator ==(Coord c1, Coord c2) {
		return c1.Equals(c2);
	}

	public static bool operator !=(Coord c1, Coord c2) {
		return !c1.Equals(c2);
	}

	public static Coord operator +(Coord c1, Coord c2) {
		return new Coord(c1.x + c2.x, c1.y + c2.y);
	}

	public override bool Equals (object obj) {
		if (obj is Coord) {
			Coord val = (Coord)obj;
			return (val.x == this.x && val.y == this.y);
		}
		return base.Equals (obj);
	}

	public override int GetHashCode () {
		return base.GetHashCode ();
	}
}
[System.Serializable]
public struct Cell {

	[XmlAttribute("can_move")]
	public bool isCanMove;

	[XmlAttribute("type")]
	public CellType type;
	
	[XmlAttribute("model")]
	public CellModel model;
	
	[XmlElement("enemy")]
	public Enemy enemy;

	[XmlAttribute("node")]
	public string nodeId;

	[XmlIgnore]
	public int marker;	// for path find
}

[System.Serializable]
public class Map {

	public static Map instance = null;
	
	[XmlAttribute("width")]
	public int width = 1;

	[XmlAttribute("height")]
	public int height = 1;

	[XmlIgnore]
	public Cell[,] cells = null;

	[XmlArray("cells")]
	public Cell[] packedCells = null; // for cells serialization

	public Cell this[Coord coord] {
		get {
			if (cells != null && coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				return cells[coord.x, coord.y];
			}
			return new Cell(){isCanMove = false};
		}
		set {
			if (cells != null && coord.x >= 0 && coord.y >= 0 && coord.x < width && coord.y < height) {
				cells[coord.x, coord.y] = value;
			}
		}
	}
	public Cell this[int x, int y] {
		get {
			if (cells != null && x >= 0 && y >= 0 && x < width && y < height) {
				return cells[x, y];
			}
			return new Cell(){isCanMove = false};
		}
	}

	public Coord[] FindPath(Coord from, Coord to, int distance = 10) {
		ClearMarks ();
		MarkNearest (from, distance);
		if (this [to].marker > 0) {
			return GetCoordPath(to, from);
		}
		return null;
	}

	Coord[] GetCoordPath(Coord to, Coord from) {
		List<Coord> path = new List<Coord> ();
		path.Add (to);
		int currentDist = this [to].marker;
		for (int i = 0; i < 100 ; i++) {
			foreach(var shift in pathShifts) {
				if (currentDist < this [to + shift].marker) {
					currentDist = this [to + shift].marker;
					to = to + shift;
					path.Add (to);
					break;
				}
			}
			if (to == from) {
				break;
			}
		}
		return path.ToArray ();
	}
	
	void ClearMarks() {
		if (cells != null) {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					cells[i, j].marker = 0;
				}
			}
		}
	}

	readonly Coord[] pathShifts = new Coord[] {
		new Coord( 1, 0),
		new Coord( 0, 1),
		new Coord(-1, 0),
		new Coord( 0,-1),
		new Coord( 1, 1),
		new Coord(-1,-1)
	};

	public void MarkNearest (Coord from, int distance = 0) {
		if (distance > 0 && this[from].isCanMove && this[from].marker < distance) {
			Cell point = this[from];
			if (point.enemy != null) { // to avoid enemies
				distance -= 2;
			}
			point.marker = distance;
			this[from] = point;
			for (int i = 0; i < pathShifts.Length; i++) {
				MarkNearest(from + pathShifts[i], distance - 1);
			}
		}
	}

	public Map () {}

	public Map (Cell[,] newCells, int newWidht = 40, int newHeight = 40) {
		if (newCells != null) {
			width = newWidht;
			height = newHeight;
			cells = newCells;
		} else {
			GenerateRandomMap (newWidht, newHeight);
		}
	}

	void GenerateRandomMap(int newWidht = 100, int newHeight = 100) {
		width = newWidht;
		height = newHeight;
		cells = new Cell[width, height];
		// Generate random map
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				cells [i, j] = new Cell ();
				cells [i, j].isCanMove = true;
				cells [i, j].type = CellType.Grass;
				// random barriers
				if (Random.Range (0, 8) == 0) {
					cells [i, j].isCanMove = false;
					cells [i, j].type = CellType.Void;
					cells [i, j].model = CellModel.Rock;
					if (Random.Range (0, 8) == 0) {
						cells [i, j].type = CellType.Water;
						cells [i, j].model = CellModel.None;
					}
				}
				// clear borders
				if (i == 0 || j == 0 || i == width - 1 || j == height - 1) {
					cells [i, j].isCanMove = false;
					cells [i, j].type = CellType.Void;
				}
				if (Random.Range (0, 7) == 0) {
					cells [i, j].isCanMove = false;
					cells [i, j].type = CellType.Void;
					cells [i, j].model = CellModel.None;
				}
				if (cells [i, j].isCanMove) {
					cells [i, j].type = CellType.Grass;
					if (Random.Range (0, 5) == 0) {
						cells [i, j].type = CellType.Desert;
					}
					if (Random.Range (0, 8) == 0) {
						cells [i, j].type = CellType.Stone;
					}
					if (Random.Range (0, 80) == 0) {
						cells [i, j].model = CellModel.City;
					}
					if (Random.Range (0, 60) == 0) {
						cells [i, j].model = CellModel.Ruins;
					}
					if (Random.Range (0, 8) == 0) {
						cells [i, j].model = CellModel.Forest;
						cells [i, j].type = CellType.Grass;
					}
					// enemy
					if (Random.Range (0, 30) == 0) {
						cells [i, j].enemy = new Enemy ();
					}
				}
			}
		}
		// hero
		cells[10, 10].isCanMove = true;
	}

	void PackCells() {
		packedCells = new Cell[width * height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				packedCells[i * height + j] = cells[i, j];
			}
		}
	}

	void UnpackCells() {
		cells = new Cell[width, height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				cells[i, j] = packedCells[i * height + j];
			}
		}
	}

	public static Map LoadFromAsset(TextAsset dataAsset) {
		if (dataAsset != null){
			XmlSerializer serialReader = new XmlSerializer(typeof(Map));
			Stream writer = new MemoryStream(dataAsset.bytes);
			Map mapInfo = (Map)serialReader.Deserialize(writer);
			writer.Close();
			mapInfo.UnpackCells ();
			return mapInfo;
		} else {
			Debug.Log("dataAsset not setted");
		}
		return null;
	}
	#if UNITY_EDITOR
	// Save file in resources only from editor
	public static void Save (Map structure, string fileName) {
		structure.PackCells ();
		XmlSerializer serialWrite = new XmlSerializer(typeof(Map));
		XmlTextWriter writer = new XmlTextWriter (fileName, System.Text.UTF8Encoding.UTF8);
		writer.Formatting = Formatting.Indented;
		serialWrite.Serialize (writer, structure);
		writer.Close();
	}
	public void SaveToAsset(TextAsset dataAsset) {
		Save (this, AssetDatabase.GetAssetPath(dataAsset));
		Debug.Log("Save map to " + AssetDatabase.GetAssetPath(dataAsset));
	}
	#endif
}
