﻿using UnityEngine;
using System.Collections;

// Hero marker on global map
public class HeroMarker : MonoBehaviour {
	static HeroMarker instance = null;

	public static HeroMarker Instance {
		get {return instance;}
	}

	[SerializeField] Animator heroAnimator = null; // fill from editor
	// for run animation
	bool isRun = false;
	const float stopAnimationStartDistance = 1.6f;
	// for marker move
	const float moveSpeed = 5f;
	float distance = 0;
	Trajectory trajectory = null;
	Vector3 previosPosition = Vector3.zero; // to get turn angle
	float needAngle = 0;
	float angle = 0;
	Action endMoveAction = null;
	
	public bool IsMoved{
		get {return (trajectory != null);}
	}

	public void SetPosition(Vector3 newPosition) {
		transform.localPosition = newPosition;
		if (CameraController.instance != null) {
			CameraController.instance.selfTransform.localPosition = transform.localPosition;
		}
	}

	public void MoveByPath(Vector3[] path, Action endAction = null) {
		if (path == null) {
			return;
		}
		trajectory = new Trajectory (path);
		distance = 0;
		endMoveAction = endAction;
	}

	void Awake () {
		instance = this;
	}

	void Update () {
		bool needRun = false;
		if (trajectory != null) { // need to move
			distance += Time.deltaTime * moveSpeed;
			Vector3 position = trajectory.GetPoint(distance);
			if (distance > trajectory.FullLenght - stopAnimationStartDistance) {
				needRun = false;
			} else {
				needRun = true;
			}
			if (distance > trajectory.FullLenght) {
				trajectory = null;
				// end Action
				if (endMoveAction != null) {endMoveAction();}
			}
			needAngle = Utils.GetAngle(new Vector2(previosPosition.x - position.x, previosPosition.z - position.z));
			previosPosition = position;
			transform.localPosition = position;
		}
		if (needAngle != angle) {
			angle = Utils.RotateToAngle(angle, needAngle, 450 * Time.deltaTime);
			transform.localEulerAngles = new Vector3(0, angle, 0);
		}
		// hero animation
		if (needRun != isRun && heroAnimator != null) {
			isRun = needRun;
			heroAnimator.SetBool ("run", isRun);
			if (isRun) {
				heroAnimator.Play("run_start");
			}
		}
		// move camera to hero position
		if (CameraController.instance != null) {
			CameraController.instance.selfTransform.localPosition = CameraController.instance.selfTransform.localPosition * 0.94f + transform.localPosition * 0.06f;
		}
	}
}
