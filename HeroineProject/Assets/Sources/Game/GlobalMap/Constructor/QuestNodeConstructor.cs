﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class QuestNodeConstructor : MonoBehaviour {
	public string id = ""; // id of node for search and selection
	public string text = ""; // main node text/text tag(for translate)
	public List<ConditionConstructor> conditions = new List<ConditionConstructor>();
	public List<ActionConstructor> actions = new List<ActionConstructor>(); // actions on activate
	public List<AnswerConstructor> answers = new List<AnswerConstructor>();

	[System.Serializable]
	public class ActionConstructor {
		public string Name { get { return name;}}
		public string name = ""; // name of trigger or item
		public QuestAction.Type type = QuestAction.Type.SetTriggerValue;
		public int value = 0; // new value of action - how many moneys give, or new trigger value
		public QuestAction GetAction() { return new QuestAction (type, name, value);}
	}

	public QuestNode GetNode(){
		return new QuestNode () {
			id = id,
			text = text,
			showConditions = GetConditions(),
			actions = GetActions(),
			answers = GetAnswers()
		};
	}

	QuestCondition[] GetConditions() {
		List<QuestCondition> conditionsList = new List<QuestCondition> ();
		foreach (var condition in conditions) {
			if (condition != null) {
				conditionsList.Add(condition.GetCondition());
			}
		}
		return conditionsList.ToArray ();
	}

	QuestAnswer[] GetAnswers() {
		List<QuestAnswer> answerList = new List<QuestAnswer> ();
		foreach (var answer in answers) {
			if (answer != null) {
				answerList.Add(answer.GetAnswer());
			}
		}
		return answerList.ToArray ();
	}
	
	QuestAction[] GetActions() {
		List<QuestAction> actionList = new List<QuestAction> ();
		foreach (var answer in actions) {
			if (answer != null) {
				actionList.Add(answer.GetAction());
			}
		}
		return actionList.ToArray ();
	}

	#if UNITY_EDITOR
	void Start () {
//		CellConstructor parent = GetComponent<CellConstructor> ();
//		if (parent != null) {
//			parent.enemy = this;
//		} else {
//			parent = GetComponentInParent<CellConstructor> ();
//			if (parent != null) {
//				parent.enemy = this;
//			}
//		}
	}

	[ContextMenu("Add Condition")]
	public void AddCondition() {
		gameObject.AddComponent<ConditionConstructor> ();
	}
	[ContextMenu("Add Answer")]
	public void AddAnswer() {
		Utils.Create<AnswerConstructor>(transform, "answer_" + answers.Count.ToString());
	}
	
	void OnDrawGizmos() {
		Gizmos.color = new Color(0.1f, 0.1f, 0.1f);
		Gizmos.DrawCube (transform.position, Vector3.one * 0.2f);
		Handles.color = Color.white;
		Handles.Label(transform.position +  new Vector3(0.4f, 0, 0), id + ": " + text + "..");
		if (answers != null) {
			for(int i = 0; i < answers.Count; i++) {
				if (answers[i] != null) {
					Gizmos.color = new Color(0.1f, 0.5f, 0.1f);
					Gizmos.DrawCube (transform.position +  new Vector3(0, -0.4f * (float)(i + 1)), Vector3.one * 0.2f);
					Handles.Label(transform.position +  new Vector3(0.4f, -0.4f * (float)(i + 1)), answers[i].answer + "..");
				}
			}
		}
	}
	#endif
}
