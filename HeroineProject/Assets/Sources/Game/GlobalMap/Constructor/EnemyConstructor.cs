﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class EnemyConstructor : MonoBehaviour {
	
	public string enemyName = "Enemy";
	public Enemy.Type type = Enemy.Type.raider;
	
	public int level = 1;
	
	public int health = 100;
	public int maximumHealth = 100;
	
	#if UNITY_EDITOR
	void Start () {
		CellConstructor parent = GetComponent<CellConstructor> ();
		if (parent != null) {
			parent.enemy = this;
		} else {
			parent = GetComponentInParent<CellConstructor> ();
			if (parent != null) {
				parent.enemy = this;
			}
		}
	}

	void OnDrawGizmos() {
		Gizmos.color = Color.red;
		Gizmos.DrawCube (transform.position + new Vector3(0, 0.5f, 0), Vector3.one);
	}
	#endif
}
