﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class ConditionConstructor : MonoBehaviour {

	public ConditionElement.Type typeA = ConditionElement.Type.Constanta;
	public string triggerNameA = "";
	public int valueA = 0;
	public ConditionElement.Type typeB = ConditionElement.Type.Constanta;
	public string triggerNameB = "";
	public int valueB = 0;
	public QuestCondition.ConditionType condition = QuestCondition.ConditionType.Equal;

	#if UNITY_EDITOR
	void Start () {
		QuestNodeConstructor parent = GetComponent<QuestNodeConstructor> ();
		if (parent != null) {
			if (!parent.conditions.Contains(this)) {
				parent.conditions.Add (this);
			}
		} else {
			AnswerConstructor aparent = GetComponent<AnswerConstructor> ();
			if (aparent != null) {
				if (!aparent.conditions.Contains(this)) {
					aparent.conditions.Add (this);
				}
			}
		}
	}

	public QuestCondition GetCondition() {
		return new QuestCondition (condition, new ConditionElement(typeA, triggerNameA, valueA), new ConditionElement(typeB, triggerNameB, valueB));
	}
	#endif
}
