﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class CellConstructor : MonoBehaviour {
	public int x = 0;
	public int y = 0;
	public CellType type = CellType.Grass;
	public CellModel model = CellModel.None;
	public EnemyConstructor enemy = null;
	public string questName = "";

	#if UNITY_EDITOR

	// Use this for initialization
	void Start () {
	}

	Vector3 currentPosition = Vector3.zero;
	
	[ContextMenu("Add Enemy")]
	public void AddEnemy() {
		Utils.Create<EnemyConstructor> (transform, "enemy");
	}

	void OnDrawGizmos () {
		Vector3 position = Level.GetHexPosition (x, y);
		if (currentPosition != position) {
			currentPosition = position;
			transform.position = position;
		}
		bool isSelected = false;
		GameObject[] selected = Selection.gameObjects;
		if (selected != null) {
			foreach(var obj in selected) {
				if (obj == gameObject) {
					isSelected = true;
					break;
				}
			}
		}
		Vector3 size = new Vector3(Level.cellSize * 0.9f,  0.2f, Level.cellSize * 0.9f);
		switch (type) {
		case CellType.Grass:
			Gizmos.color = new Color(0.2f, 0.6f, 0.2f);
			break;
		case CellType.Desert:
			Gizmos.color = Color.yellow;
			break;
		case CellType.Water:
			Gizmos.color = Color.blue;
			break;
		case CellType.Stone:
			Gizmos.color = new Color(0.4f, 0.4f, 0.4f);
			size = new Vector3(Level.cellSize * 0.9f,  0.8f, Level.cellSize * 0.9f);
			break;
		case CellType.Void:
			Gizmos.color = Color.black;
			size = new Vector3(Level.cellSize * 0.6f,  0.1f, Level.cellSize * 0.6f);
			break;
		}
		if (isSelected) {
			Gizmos.DrawCube (currentPosition, size * 0.4f);
		} else {
			Gizmos.DrawCube (currentPosition, size);
		}
		switch (model) {
		case CellModel.Rock:
			Gizmos.color = new Color(0.4f, 0.4f, 0.4f);
			Gizmos.DrawCube (currentPosition, Vector3.one * 0.8f);
			break;
		case CellModel.Ruins:
			Gizmos.color = new Color(0.4f, 0.4f, 0.4f);
			Gizmos.DrawCube (currentPosition, new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0.3f, 0, 0), new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0, 0, 0.3f), new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0.3f, 0, 0.3f), new Vector3(0.3f, 0.8f, 0.3f));
			break;
		case CellModel.City:
			Gizmos.color = new Color(1f, 1f, 1f);
			Gizmos.DrawCube (currentPosition, new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0.3f, 0, 0), new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0, 0, 0.3f), new Vector3(0.3f, 0.8f, 0.3f));
			Gizmos.DrawCube (currentPosition + new Vector3(0.3f, 0, 0.3f), new Vector3(0.3f, 0.8f, 0.3f));
			break;
		case CellModel.Forest:
			Gizmos.color = new Color(0.2f, 0.9f, 0.2f);
			Gizmos.DrawCube (currentPosition, new Vector3(0.3f, 0.8f, 0.3f));
			break;
		}
	}
	#endif
}
