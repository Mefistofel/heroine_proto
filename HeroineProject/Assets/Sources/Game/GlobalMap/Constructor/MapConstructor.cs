﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class MapConstructor : MonoBehaviour {

	public Vector3 mapShift = Vector3.zero;

	public int width = 40;
	public int height = 40;
	public CellConstructor[] cells = null;
	
	public int RandomVoidCount = 0;
	public int RandomDesertCount = 0;
	public int RandomStoneCount = 0;
	public int RandomWaterCount = 0;
	public int RandomForestCount = 0;
	public int RandomRockCount = 0;


	public Cell[,] GetCells() {
		Cell[,] map = null;
		if (cells != null) {
			map = new Cell[width, height];
			for(int i = 0; i < width; i++) {
				for(int j = 0; j < height; j++) {
					map[i, j] = new Cell();
					if (cells[i * height + j] != null) {
						map[i, j].type = cells[i * height + j].type;
						map[i, j].model = cells[i * height + j].model;
						map[i, j].nodeId = cells[i * height + j].questName;
						map[i, j].isCanMove = !(
							cells[i * height + j].type == CellType.Void ||
							cells[i * height + j].model == CellModel.Rock ||
							cells[i * height + j].type == CellType.Water
							);
						if (cells[i * height + j].enemy != null) {
							map[i, j].enemy = new Enemy() {
								name = cells[i * height + j].enemy.enemyName,
								type = cells[i * height + j].enemy.type,
								level = cells[i * height + j].enemy.level,
								health = cells[i * height + j].enemy.health,
								maximumHealth = cells[i * height + j].enemy.maximumHealth
							};
						}
					}
				}
			}
		}
		return map;
	}

	void Awake () {
		Level.shift = mapShift;
	}

	#if UNITY_EDITOR
	
	public TextAsset levelFile = null;
	public bool needSave = false;

	public int corners = 20;
	
	void Start () {
		Level.shift = mapShift;
	}

	void Update () {
	
	}

	[ContextMenu("Recreate")]
	public void Recreate() {
		Level.shift = mapShift;
		cells = null;
		List<GameObject> childrens = new List<GameObject>();
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			childrens.Add(child.gameObject);
		}
		foreach (var child in childrens) {
			DestroyImmediate(child);
		}
		// CreateRegions
		const int regionSize = 8;
		int regionX = width / regionSize;
		int regionY = height / regionSize;
		GameObject[,] Regions = new GameObject[regionX, regionY];
		for (int i = 0; i < regionX; i++) {
			for (int j = 0; j < regionY; j++) {
				Regions[i, j] = Utils.CreateGameObject(transform, Level.GetHexPosition (i * regionSize, j * regionSize), "region_" + i.ToString() + "_" + j.ToString());
			}
		}
		// CreateCells
		cells = new CellConstructor[width * height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				cells[i * height + j] = Utils.Create<CellConstructor>(Regions[i / regionSize, j / regionSize].transform, "cell_" + i.ToString() + "_" + j.ToString());
				cells[i * height + j].x = i;
				cells[i * height + j].y = j;
				// clear borders
				if (i == 0 || j == 0 || i == width - 1 || j == height - 1) {
					cells [i * height + j].type = CellType.Void;
				}
				// clear corners
				if (i - j < corners - width) {
					cells [i * height + j].type = CellType.Void;
				}
				if (j - i < corners - height) {
					cells [i * height + j].type = CellType.Void;
				}
			}
		}
		// Add random values
		if (RandomDesertCount > 0) {
			for(int i = 0; i < RandomDesertCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				if (cells [x * height + y].type != CellType.Void) {
					cells [x * height + y].type = CellType.Desert;
				}
			}
		}
		if (RandomStoneCount > 0) {
			for(int i = 0; i < RandomStoneCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				if (cells [x * height + y].type != CellType.Void) {
					cells [x * height + y].type = CellType.Stone;
				}
			}
		}
		if (RandomWaterCount > 0) {
			for(int i = 0; i < RandomWaterCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				if (cells [x * height + y].type != CellType.Void) {
					cells [x * height + y].type = CellType.Water;
				}
			}
		}
		if (RandomForestCount > 0) {
			for(int i = 0; i < RandomForestCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				if (cells [x * height + y].type != CellType.Void) {
					cells [x * height + y].type = CellType.Grass;
					cells [x * height + y].model = CellModel.Forest;
				}
			}
		}
		if (RandomRockCount > 0) {
			for(int i = 0; i < RandomRockCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				if (cells [x * height + y].type != CellType.Void) {
					cells [x * height + y].type = CellType.Void;
					cells [x * height + y].model = CellModel.Rock;
				}
			}
		}
		if (RandomVoidCount > 0) {
			for(int i = 0; i < RandomVoidCount; i++) {
				int x = Random.Range(0, width - 1);
				int y = Random.Range(0, height - 1);
				cells [x * height + y].type = CellType.Void;
				cells [x * height + y].model = CellModel.None;
			}
		}
	}
	[ContextMenu("Save to file")]
	public void SaveToFile() {
		Map map = new Map(GetCells());
		map.SaveToAsset(levelFile);
	}

	void OnDrawGizmos() {
		Level.shift = mapShift;
		Vector3 TopRightCorner = Level.GetHexPosition (width - 1, height - 1);
		Vector3 TopLeftCorner = Level.GetHexPosition (width - 1, 0);
		Vector3 BottomRightCorner = Level.GetHexPosition (0, height - 1);
		Vector3 BottomLeftCorner = Level.GetHexPosition (0, 0);
		Gizmos.color = Color.green;
		Gizmos.DrawLine (TopRightCorner, TopLeftCorner);
		Gizmos.DrawLine (TopLeftCorner, BottomLeftCorner);
		Gizmos.DrawLine (BottomLeftCorner, BottomRightCorner);
		Gizmos.DrawLine (BottomRightCorner, TopRightCorner);
		if (needSave) {
			if (levelFile != null) {
				SaveToFile();
			}
			needSave = false;
		}
	}
	#endif
}
