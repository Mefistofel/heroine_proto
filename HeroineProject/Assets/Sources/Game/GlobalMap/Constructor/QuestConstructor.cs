﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class QuestConstructor : MonoBehaviour {
	public static QuestConstructor instance = null;
	[System.Serializable]
	public class TriggerParams {
		public string Name { get { return name;}}
		public string name;
		public int value;
	}
	void Start () {
		instance = this;
	}
	#if UNITY_EDITOR
	public TextAsset questsFile = null;
	public bool needSave = false;
	public TriggerParams[] triggers = null;

	QuestNode[] GetNodes() {
		QuestNodeConstructor[] constructors = GameObject.FindObjectsOfType<QuestNodeConstructor> ();
		if (constructors == null || constructors.Length == 0) {
			return null;
		}
		QuestNode[] nodes = new QuestNode[constructors.Length];
		for (int i = 0; i < constructors.Length; i++) {
			nodes[i] = constructors[i].GetNode();
		}
		return nodes;
	}

	[ContextMenu("Save to file")]
	public void SaveToFile() {
		if (questsFile != null) {
			QuestLog quests = new QuestLog ();
			QuestNode[] nodes = GetNodes();
			quests.nodes.AddRange(nodes);
			quests.SaveToAsset(questsFile);
		} else {
			Debug.LogWarning("quests file not set");
		}
	}
	
	void OnDrawGizmos() {
//		Gizmos.color = Color.gray;
//		Gizmos.DrawCube (transform.position, Vector3.one);
		if (needSave) {
			SaveToFile();
			needSave = false;
		}
	}
	#endif
}
