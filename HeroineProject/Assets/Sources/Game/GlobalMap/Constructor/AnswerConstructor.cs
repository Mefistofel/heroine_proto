﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
[ExecuteInEditMode]
#endif
public class AnswerConstructor : MonoBehaviour {

	public string answer = "answer"; // answer text/text tag(for translate)
	public string nextNode = ""; // next node selected after that answer
	public List<ConditionConstructor> conditions = new List<ConditionConstructor>();

	#if UNITY_EDITOR
	
	public QuestAnswer GetAnswer(){
		return new QuestAnswer (answer, nextNode, GetActions(),GetConditions());
	}

	QuestCondition[] GetConditions() {
		List<QuestCondition> conditionsList = new List<QuestCondition> ();
		foreach (var condition in conditions) {
			if (condition != null) {
				conditionsList.Add(condition.GetCondition());
			}
		}
		return conditionsList.ToArray ();
	}

	QuestAction[] GetActions() {
		return null;
	}
	
	[ContextMenu("Add Condition")]
	public void AddCondition() {
		gameObject.AddComponent<ConditionConstructor> ();
	}

	void Start () {
		QuestNodeConstructor parent = GetComponentInParent<QuestNodeConstructor> ();
		if (parent != null) {
			if (!parent.answers.Contains(this)) {
				parent.answers.Add (this);
			}
		} else {
		}
	}
	#endif
}
