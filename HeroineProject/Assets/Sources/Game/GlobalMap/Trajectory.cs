﻿using UnityEngine;

public class Trajectory {
	// for path move
	Vector3[] path = null;
	float[] pathSegmentLenghts = null;
	float[] pathSegmentDistances = null;
	float fullLenght = 0;
	
	public float FullLenght {
		get {
			return fullLenght;
		}
	}
	
	public Trajectory(Vector3[] pathPoints) {
		path = pathPoints;
		pathSegmentDistances = null;
		pathSegmentLenghts = null;
		fullLenght = 0;
		if (path != null && path.Length > 0) {
			pathSegmentLenghts = new float[path.Length];
			pathSegmentDistances = new float[path.Length];
			for(int i = 0; i < path.Length - 1; i++) {
				pathSegmentLenghts[i] = (path[i] - path[i + 1]).magnitude;
				pathSegmentDistances[i] = fullLenght;
				fullLenght += pathSegmentLenghts[i];
			}
			pathSegmentLenghts[path.Length - 1] = 0;
			pathSegmentDistances[path.Length - 1] = fullLenght;
		}
	}
	
	public Vector3 GetPoint(float distance) {
		if (path == null) {
			return Vector3.zero;
		}
		if (path.Length == 0) {
			return path[0];
		}
		if (distance < 0) {
			return path[0];
		}
		if (distance >= fullLenght) {
			return path[path.Length - 1];
		}
		for (int i = 0; i < path.Length - 1; i++) {
			if (distance <= pathSegmentDistances[i + 1]) {
				float segment = (distance - pathSegmentDistances[i]) / pathSegmentLenghts[i];
				return Vector3.Lerp(path[i], path[i + 1], segment);
			}
		}
		return path[path.Length - 1];
	}
}
