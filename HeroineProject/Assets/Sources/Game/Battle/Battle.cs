﻿using UnityEngine;
using System.Collections;

public class Battle: MonoBehaviour {

	public struct Result {
		public bool isWin;
		public Result(bool win) {
			isWin = win;
		}
	}

	public delegate void BattleResultDelegate(Result result);

	static BattleResultDelegate battleResult = null;

	public static void StartBattle(Enemy enemy, BattleResultDelegate result) {
		battleResult = result;
		// start battle
		CameraController.AnimateZoom (-10, -2);
		LoadingFader.Show(() => {
			Application.LoadLevel("BattleScene");
			CameraController.AnimateZoom (-25, -6, 1f);
			LoadingFader.Hide();
			// animate rotation
			Timer.Add(1f, (anim) => {
				if (CameraController.instance != null) {
					anim = 1f - (anim) * (anim);
					CameraController.instance.transform.localEulerAngles = new Vector3(30f + 15f * anim, 30f - 120f * anim);
				}
			}, () => {
				if (CameraController.instance != null) {
					CameraController.instance.transform.localEulerAngles = new Vector3(30f, 30f);
				}
			});
		});
	}

	public static void EndBattle(Result result) {
		CameraController.AnimateZoom (-6, -25);
		LoadingFader.Show(() => {
			Application.LoadLevel("MainScene");
			CameraController.AnimateZoom (-2, -10);
			if (battleResult != null) {
				battleResult(result);
				battleResult = null;
			}
			LoadingFader.Hide();
		});
	}

	void OnGUI(){
		if (GUI.Button(Utils.ScreenQuadRect(30f, 5f), "Win")) {
			EndBattle(new Result(true));
		}
		if (GUI.Button(Utils.ScreenQuadRect(50f, 5f), "Lose")) {
			EndBattle(new Result(false));
		}
	}
}
