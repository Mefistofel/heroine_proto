﻿using UnityEngine;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;

// structure for enemy parameters
[System.Serializable]
public class Enemy {
	[System.Serializable]
	public enum Type {
		raider,
		bird,
		worm,
		dragon
	}
	[XmlAttribute("name")]
	public string name = "Barmaglot";
	[XmlAttribute("type")]
	public Type type = Type.raider;
	
	[XmlAttribute("lev")]
	public int level = 1;
	
	[XmlAttribute("hp")]
	public int health = 100;
	[XmlAttribute("mhp")]
	public int maximumHealth = 100;

	public Enemy() {}
}
