﻿using UnityEngine;
using System.Collections;

public class LoadingFader : MonoBehaviour {
	public static LoadingFader instance;
	public Sprite loadSprite = null;
	SpriteRenderer faderRenderer;
	Color color = Color.black;
	bool show = false;
	float showTimer = 0;
	const float showTime = 0.4f;
	Action action;

	void Awake () {
		if (instance != null) {
			instance.show = false;
			Destroy(gameObject);
			return;
		}
		instance = this;
		color.a = 0;
		faderRenderer = Utils.GetOrCreateComponent<SpriteRenderer>(gameObject);
		faderRenderer.sprite = GetTexture ();
		faderRenderer.color = color;
		faderRenderer.gameObject.layer = Interface.INTERFACE_LAYER;
		gameObject.SetActive (false);
		DontDestroyOnLoad (gameObject);
	}

	public Sprite GetTexture() {
		if (loadSprite != null) {
			return loadSprite;
		} else {
			Texture2D blankTexture = new Texture2D (16, 16, TextureFormat.RGB24, true);
			Color[] colors = new Color[16 * 16];
			for (int i = 0; i < colors.Length; i++) {
				colors [i] = Color.white;
			}
			blankTexture.SetPixels (colors);
			blankTexture.Apply ();
			Sprite blank = Sprite.Create (blankTexture, new Rect (0, 0, 16, 16), new Vector2 (0.5f, 0.5f), 0.5f);
			return blank;
		}
	}
	public static void Show(Action action, Vector3? position = null) {
		if (instance != null) {
			instance.transform.localPosition = new Vector3(0,0,-9.5f);
			instance.gameObject.SetActive (true);
			if (position != null) {
				instance.transform.localPosition = position.Value;
			}
			instance.action = action;
			instance.show = true;
		} else {
			Utils.Create<LoadingFader>("Fader");
			LoadingFader.Show(action, position);
		}
	}
	
	public static void Hide() {
		if (instance != null) {
			instance.show = false;
		}
	}

	void Update () {
		if (show) {
			if (showTimer < showTime) {
				showTimer += Time.deltaTime;
				if (showTimer >= showTime) {
					showTimer = showTime;
					if (action != null) {
						action();
					}
				}
				color.a = showTimer / showTime * 3f;
				faderRenderer.color = color;
			}
		} else {
			if (showTimer > 0f) {
				showTimer -= Time.deltaTime;
				if (showTimer <= 0f) {
					showTimer = 0f;
					instance.gameObject.SetActive (false);
				}
				color.a = showTimer / showTime * 3f;
				faderRenderer.color = color;
			}
		}
	}
}
