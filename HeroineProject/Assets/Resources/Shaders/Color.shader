Shader "Added/Colored" {
Properties {
	_Color ("Color", Color) = (1,1,1)
}

Category {
	Tags {"Queue"="Transparent" "IgnoreProjector"="True"}
	Lighting Off Cull Back ZTest Always ZWrite On 
	
	SubShader {
		Pass {
			Color [_Color]
		}
	}
}
}