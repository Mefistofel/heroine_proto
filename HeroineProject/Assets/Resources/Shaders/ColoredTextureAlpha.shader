Shader "Added/TexturedAlpha" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Main Tex", 2D) = "white"
    }
	Category {
		Tags {Queue=Transparent}
		Lighting Off
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		SubShader {	
			Pass {
			   SetTexture [_MainTex] {
			   		ConstantColor [_Color]
					Combine texture * constant, texture * constant 
			    }
			}
        } 
    }
}