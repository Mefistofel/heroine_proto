﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(InterfaceButton))]
public class InterfaceButtonEditorHelper : Editor
{
	string[] methodList = null;
	int selectedMethod = 0;
	
	public override void OnInspectorGUI () {
		// Draw the default inspector
		DrawDefaultInspector();
		var button = target as InterfaceButton;
		button.TryToFindOwner ();
		methodList = button.GetOwnerMethodNames ();
		selectedMethod = EditorGUILayout.Popup(selectedMethod, methodList);
		// Update the selected choice in the underlying object
		if (methodList != null && methodList.Length > 0 && selectedMethod < methodList.Length && selectedMethod > 0) {
			button.SetMethodName(methodList[selectedMethod]);
			// Save the changes back to the object
			EditorUtility.SetDirty(target);
		}
	}
}