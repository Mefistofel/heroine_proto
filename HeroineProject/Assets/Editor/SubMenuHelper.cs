﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(QuestNodeConstructor))]
public class SubMenuHelper : Editor {
	
	public override void OnInspectorGUI () {
		// Draw the default inspector
		DrawDefaultInspector();
		var component = target as QuestNodeConstructor;
		
		var methods = component.GetType().GetMethods();
		foreach(var method in methods) {
			if (method.DeclaringType == typeof(QuestNodeConstructor)) {
				object[] attr = method.GetCustomAttributes(false);
				foreach(var obj in attr) {
					if (obj is UnityEngine.ContextMenu) {
						//					Debug.Log("Context menu " + method.Name);
						Rect r = EditorGUILayout.BeginHorizontal ("Button");
						if (GUI.Button (r, GUIContent.none)) {
							method.Invoke(component, null);
						}
						GUILayout.Label (method.Name);
						EditorGUILayout.EndHorizontal ();
					}
				}
			}
		}
	}
}
